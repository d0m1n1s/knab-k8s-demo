package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func main() {
	time.Sleep(10)

	for n := 0; n <= 100; n++ {
		url := fmt.Sprintf("http://color:9000/?%d", n)
		fmt.Printf("getting %s\n", url)
		resp, err := http.Get(url)

		if err != nil {
			panic(err)
		}

		defer resp.Body.Close()

		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", data)
	}
}
