package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	randNum := rand.Intn(16777215)
	randColor := strconv.FormatInt(int64(randNum), 16)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, randColor)
		// fmt.Fprintf(w, "red")
	})

	http.ListenAndServe(":8080", nil)
}
