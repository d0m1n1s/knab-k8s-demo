# k8s demo

### howto run:

```
# install minikube
brew install minikube
# start minikube
minikube start

# install skaffold
brew install skaffold

# clone this repo
cd /tmp
git clone https://gitlab.com/d0m1n1s/knab-k8s-demo.git
cd knab-k8s-demo

# initialize skaffold (select with enter which projects you want to include)
skaffold init

# run skaffold
skaffold dev
```

### example commands:

```
# get services
kubectl get svc

# get list of pods
kubectl get pod

# get pod logs
kubectl logs <podname>
```

### some experiments:

- you can change the number of `replicas` in `service-color/kubernetes/deployment.yaml`
- you can uncomment the `red` line in `service-color/main.go`
